<?Php
$servername = "localhost";
$database = "dacon";
$username = "root";
$password = "";

//echo "$servername";exit;
$conn = mysqli_connect($servername, $username, $password, $database);
// Check connection
if (!$conn) {
	die("Connection failed: " . mysqli_connect_error());
}

$data = json_decode(file_get_contents('php://input'), true);
//output response
$resp=array();
if(isset($data['data'])){
	foreach($data['data'] as $x=>$v){
		$sql="INSERT INTO tbl_data_kamera (ip_camera,tgl,jam,cl_kategori_id,create_date,create_by,file,file_name) VALUES ('".$v["ip_camera"]."','".$v["tgl"]."','".$v["jam"]."','".$v["cl_kategori_id"]."','".date('Y-m-d H:i:s')."','SYS','".mysqli_real_escape_string($conn, $v["file"])."','".$v["file_name"]."')";
		$conn->query($sql);
	}
	$resp["sts"]=1;
	$resp["msg"]='Data Was Submit';
}else{
	$resp["sts"]=0;
	$resp["msg"]='No Data';
}

mysqli_close($conn);
echo json_encode($resp, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);