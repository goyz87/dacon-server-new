<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service extends CI_Controller {
	function __construct(){
        parent::__construct();
		$this->host	= $this->config->item('base_url');
		$this->smarty->assign('host',$this->host);
		$this->load->model('mmodul');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
	//	echo $this->config->item('base_url');
		$this->load->view('tes2');
	}
	function tes3()
	{
	//	echo $this->config->item('base_url');
		$this->load->view('tes3.html');
	}
	function service_tp50(){
		//echo __DIR__;exit;
		$this->db->trans_begin();
		$path=$this->db->get('tbl_seting_path')->row_array();
		$dir    = $path["path_ftp"];
		$dir_repo    = $path["path_cut"];
		$file = scandir($dir, 1);
		$jml_file=count($file);
		$sts=0;
		//CUT FILE
		
		//if($sts==1){
			//MASUK DB
			
			$list=$this->getDirContents($dir,$path);
			//echo "<pre>";print_r($list);exit;
			foreach($list as $a=>$b){
				//echo $b['key'];exit;
				$ck=$this->db->get_where('tbl_data_kendaraan',array('plat_number'=>$b["plat_number"],'capture_date'=>$b["capture_date"]))->row_array();
				if(isset($ck["plat_number"])){
					$this->db->update('tbl_data_kendaraan',$b,array('plat_number'=>$b["plat_number"],'capture_date'=>$b["capture_date"]));
					echo "Update Data Berhasil Masuk Plat -> ".$b["plat_number"]." <br>";
				}else{
					$this->db->insert('tbl_data_kendaraan',$b);
					echo "Data Baru Berhasil Masuk Plat -> ".$b["plat_number"]." <br>";
				}
			}
			//END MASUK DB
		//}
		
		if($jml_file > 2 ){
			foreach($file as $v=>$x){
				if($x!="." && $x!=".."){
					/*$ext = pathinfo($x, PATHINFO_EXTENSION);
					echo $ext."<br>";
					if($ext=='csv'){
						$exp=$this->csv();
						if($exp==1){
							echo "Export Sukses";
						}
					}
					
					*/
					$this->rename_win($dir.$x, $dir_repo.$x);
					//chmod($dir.$x,0777);
					//rename($dir.$x, $dir_repo.$x);
					echo $x." -> Status : Sukses <br>";
					$sts=1;
				}
			}
		}else{
			echo "Tidak Ada File Untuk Diexport";
		}
		//END CUT
		
		
		if($this->db->trans_status() == false){
			$this->db->trans_rollback();
			echo "DAta Gagal Masuk";
		}else{
			$this->db->trans_commit();	
			echo "Semua Data Berhasil Masuk ";
		}
	}
	function getDirContents($dir,$dir_repo,&$results = array()) {
		//print_r($dir_repo);exit;
		
		$dir_ftp=$dir_repo["path_ftp"];
		$dir_rep=$dir_repo["path_cut"];
		//$dir_ftp=str_replace("/","\",$dir_ftp)
		//echo $dir_rep;exit;
		$files = scandir($dir);
		//echo $dir;exit;
		foreach ($files as $key => $value) {
			//$path_repo = realpath($dir_repo . DIRECTORY_SEPARATOR . $value);
			$path = realpath($dir . DIRECTORY_SEPARATOR . $value);
			if (!is_dir($path)) {
				
				$ex=explode("_",$value);
				$pc=explode('.',$ex[4]);
				//echo $dir_rep;exit;
				
				$path_repo =str_replace($dir_ftp,$dir_rep,$path);
				//echo $path_repo;exit;
				if($pc[0]=='MOBIL1'){
					
					$dt=array('plat_number'=>$ex[2],'capture_date'=>$ex[0],'capture_time'=>$ex[1],'speed'=>$ex[3]);
					$dt["file_foto"]=$path_repo;
				}else{
					
					$dt=array('plat_number'=>$ex[3],'capture_date'=>$ex[0],'capture_time'=>$ex[1],'speed'=>$ex[2]);
					$dt["file_flat"]=$path_repo;
				}
				
				$results[] = $dt;
				//$results[] = $path;
			} else if ($value != "." && $value != "..") {
				$this->getDirContents($path,$dir_repo, $results);
				//$results[] = $path;
			}
		}

		return $results;
	}
	function rename_win($oldfile,$newfile) {
    if (!rename($oldfile,$newfile)) {
        if (copy ($oldfile,$newfile)) {
            unlink($oldfile);
            return TRUE;
        }
        return FALSE;
    }
    return TRUE;
}
	function csv(){
		$dir_repo    = 'E:/MY PROJECT/ARDHANI/TES/';
		$file = "data.csv";
		$data=fopen($dir_repo.$file,'r');
		$row = 1;
		$dt=array();
		$this->db->trans_begin();
		 while (($column = fgetcsv($data, 10000, ",")) !== FALSE) {
			//echo "<pre>";print_r($column[0]);
			if($row > 1){
				
				//echo $column[1];exit;
				$dt[]=array('plat_number'=>$column[0],
							'car_type'=>$column[1],
							'direction'=>$column[2],
							'chanel_no'=>$column[3],
							'list_type'=>$column[4],
							'capture_time'=>$column[5],
							'speed'=>$column[6],
							'lane_no'=>$column[7],
							'data_type'=>$column[8],
							'file_flat'=>'20210223_114822525_1_B2269SIB_plate.jpg',
							'file_foto'=>'20210223_114822525_1_B2269SIB_P1.jpg',
				);
			}
			$row++;
		  }
		 // echo "<pre>";print_r($dt);
		  $this->db->insert_batch("tbl_data_kendaraan",$dt);
		  if($this->db->trans_status() == false){
			$this->db->trans_rollback();
			echo 0;
		}else{
			echo $this->db->trans_commit();	
		}
	}
	function run_kamera($ip="",$port="",$user="",$pwd="",$path=""){
		$sh='taskkill /F /IM "ffmpeg.exe"';
		shell_exec($sh); // find and kill   
		
		$awal="D:/xampp/htdocs/vms/__assets/ffmpeg/bin/ffmpeg  ";
		$tgh=" -v verbose -vf scale=1920:1080  -vcodec libx264 -r 25 -b:v 2600k -crf 31 -acodec aac  -sc_threshold 0 -f hls  -hls_time 10  -segment_time 5 -hls_list_size 6 ";
		$kamera=$this->db->get('tbl_server_camera')->result_array();
		foreach($kamera as $x=>$v){
			
			$fullPath = FCPATH . "__repo/stream/".$v["ip"]."/" ;
			array_map('unlink', glob( "$fullPath*.ts"));
			$rtsp =" -i rtsp://".$v["usr"].":".$v["pwd"]."@".$v["ip"].":".$v["port"]."/Streaming/Channels/2 -map ".$x." ";
			$path="D:/xampp/htdocs/vms/__repo/stream/".$v["ip"]."/streaming_.m3u8 ";
			if($x==0){
				$sh=$awal.$rtsp.$tgh.$path;
			}else{
				$sh .=$rtsp.$tgh.$path;
			}
			
		}
		echo shell_exec($sh);
		echo "<br>Done.\n";
		sleep(60); // Wait long enough before killing, otherwise the video will be corrupt!

		shell_exec('pkill ffmpeg'); // find and kill    
		echo "done\n\n";
		
		
		
		//echo $sh;exit;
		
		//CLEAR FILE
		
		
		/*$ip="10.4.1.181";
		$port="554";
		$user="admin";
		$pwd="hik12345";
		$path="D:/xampp/htdocs/vms/__repo/stream/streaming_.m3u8";
		$sh="D:/xampp/htdocs/vms/__assets/ffmpeg/bin/ffmpeg -v verbose  -i rtsp://$user:$pwd@$ip:$port/Streaming/Channels/2 -vf scale=1920:1080  -vcodec libx264 -r 25 -b:v 2600k -crf 31 -acodec aac  -sc_threshold 0 -f hls  -hls_time 10  -segment_time 5 -hls_list_size 6 $path ";
		echo "Starting ffmpeg...\n\n <br>";
		//echo shell_exec("D:/xampp/htdocs/vms/__assets/ffmpeg/bin/stream.bat");
		echo shell_exec($sh);
		echo "<br>Done.\n";
		sleep(60); // Wait long enough before killing, otherwise the video will be corrupt!

		shell_exec('pkill ffmpeg'); // find and kill    
		echo "done\n\n";
		*/
		//exec($sh);
	}
	function stop_kamera(){
		//END SERVICE
		$sh='taskkill /F /IM "ffmpeg.exe"';
		shell_exec($sh); // find and kill    
		//CLEAR FILE
		$kamera=$this->db->get('tbl_server_camera')->result_array();
		foreach($kamera as $x=>$v){
		$fullPath = FCPATH . "__repo/stream/".$v["ip"]."/" ;
		//echo $fullPath;exit;
		array_map('unlink', glob( "$fullPath*.ts"));
		
		echo "done\n\n";
		}
		exit();
	}
}
